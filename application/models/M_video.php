<?php
class M_video extends CI_Model
{

	function get_all_video()
	{
		$hasil = $this->db->get('tbl_video')->result_array();
		return $hasil;
	}


	function simpan_video($video_judul, $video_link, $video_keterangan)
	{

		// $hsl = $this->db->query("INSERT INTO tbl_video (video_judul, video_link, video_keterangan) VALUES ('$video_judul','$video_link','$video_keterangan'");
		// return $hsl;

		$data = array(
			'video_judul' => $video_judul,
			'video_link' => $video_link,
			'video_keterangan' => $video_keterangan

		);
		$this->db->insert('tbl_video', $data);
	}


	//UPDATE VIDEO//
	function update_video($kode, $video_judul, $video_link, $video_keterangan)
	{
		//$hsl = $this->db->query("UPDATE tbl_video set video_judul='$video_judul',video_link='$video_link',keterangan='$keterangan', where video_id='$kode'");
		//return $hsl;
		$data = array(
			'video_judul' => $video_judul,
			'video_link' => $video_link,
			'video_keterangan' => $video_keterangan

		);
		$this->db->where('video_id', $kode);
		$this->db->update('tbl_video', $data);
	}

	//END UPDATE video//

	function hapus_video($kode)
	{
		$hsl = $this->db->query("DELETE FROM tbl_video where video_id='$kode'");
		return $hsl;
	}

	function get_video_byid($video_id)
	{
		$hsl = $this->db->query("select * from tbl_video where kategori_id='$video_id'");
		return $hsl;
	}
}
