<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?></title>
    <!-- Bootstrap CSS -->

    <<link href="<?= base_url('assets/'); ?>fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url('assets/'); ?>css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light  fixed-top ">
        <div class="container-fluid ">
            <a class="navbar-brand" href="index.html">
                <img src="<?= base_url('assets/'); ?>img/moa.png" alt="" width="150" height="50" class="d-inline-block align-top">
                <img src="<?= base_url('assets/'); ?>img/mr.png" alt="" width="150" height="50" class="d-inline-block align-top">
            </a>
            <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon toggler"></span>
            </button>
            <div style="font-size: 15px;" class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url('home'); ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('home/aboutus'); ?>">Tentang Kami</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('testimoni/'); ?>">Testimoni</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('berita/berita') ?>">Berita Milagros</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://wa.me/6282178603781/?text=Hallo%20Admin%20Saya%20Ingin%20mengetahui%20milagros%20lebih%20banyak%20lagi">Kontak</a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>