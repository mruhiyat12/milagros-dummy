<div class="container-fluid content" style="min-height: 50em; margin-top: 3%;  background-color:  rgb(52, 35, 68);">
    <div class="tentangkami ">
        <h1 class="text-center">TENTANG KAMI</h1>
        <div class="mb-4">
            <img class="aboutus" src="<?= base_url('assets/'); ?>img/Capture.PNG" class="d-block w-100" alt="...">
            <h4>Selamat Datang dikeluarga Milagros</h4>
            <h5>Artipenting Tetang Kita</h5>
            <p> "Kami dan Anda adalah bagian yang tak terpisahkan dalam sebuah komunitas yang disebut
                MASYARAKAT"</br>

                "Ada banyak kesamaan tujuan yang ingin dicapai oleh seluruh anggota masyarakat"</br>

                JIKA : "KELUAR dari MASALAH" adalah tujuan Anda, maka jelaslah sudah titik terang akan ARTI PENTING
                dari PERSAHABATAN KITA</br>

                "Kenapa kita tidak bersinergi menyatukan kekuatan kita untuk mencapai tujuan bersama?"

                </br> "Kami percaya bahwa : hanya "SIMBIOSIS MUTUALISME-lah" yang dapat menjamin kelestarian sebuah
                "PERSAHABATAN"</br>

                PT Milagros Indonesia Megah adalah sebuah perusahaan yang bergerak di bidang Direct Selling. Selain
                memasarkan produknya, perusahaan juga memberi kesempatan kepada masyarakat luas untuk membangun dan
                memiliki bisnisnya sendiri. Jiwa entrepreneurship dan kemandirian ekonomi adalah inspirasi besar
                yang menjiwai latar belakang berdirinya perusahaan ini.</br>

                Demi mengawal kelancaran program yang diusungnya, perusahaan ini berkomitmen untuk menghadirkan
                produk yang berkualitas serta jelas fungsi dan manfaatnya. Selain itu dalam program yang ditawarkan,
                asas "Win Win Solution" adalah landasan dasar yang dipilih demi membangun kesejahteraan perusahaan
                dan para mitranya.</br>

                Salam Sehat, Sukses,</br>
                dan Sejahtera untuk kita semua.</p>
        </div>
        <!-- justify-content-center -->


    </div>

</div>