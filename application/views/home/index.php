 <!-- content -->
 <div class="carousel">
     <div class="carousel">
         <div class=" row carousel-content  ">
             <div class="col-md-6 text align-self-center">

                 <h1 style=" color: white;">Milagros</h1>
                 <h1 style="color: rgb(1, 185, 241);">Selamat Datang di Sentramilagros. Anda akan terhubung dengan
                     agen resmi milagros terdekat diseluruh indonesia untuk mendapatkan produk milagros dan mera yang
                     asli</h1>
                 <p style="color: white;">3 Syarat Sehat Sempurna : Sehat Rohani, Sehat Jasmani, dan Sehat Finansial
                 </p>
                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary">Beli Sekarang</a>
             </div>

             <div class="col-md-6 image ">
                 <img src="assets/img/ml1.png" alt="">
             </div>
         </div>
     </div>
     </br>
 </div>




 <div class="container-fluid content" style="min-height: 10em;  background-color:  rgb(52, 35, 68);">
     <div class="navbar-tabs text-center">
         <h1>TENTANG PRODUK</h1>
         <div class="container mb-4">
             <p>Milagros adalah air dengan pH / kadar ke-Alkalian-nya sangat stabil ( Super
                 Stable
                 Akaline ), memiliki
                 kandungan Anti-Oksidan yang tinggi ( Strenght Anti-Oxidant, dan memiliki Energy Scalar yang cukup
                 kuat (
                 Powerful Scalar Energy ).</p>
         </div>
         <!-- justify-content-center -->
         <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
             <li class="nav-item" role="presentation">
                 <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">PRODUK
                 </button>
             </li>

             <li class="nav-item" role="presentation">
                 <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">MANFAAT</button>
             </li>

         </ul>
         <div class="tab-content" id="myTabContent">
             <div class=" tab-pane fade show active mt-4" id="home" role="tabpanel" aria-labelledby="home-tab">
                 <div class="container">
                     <div class="row">
                         <div class=" col-md-4 tab-content-produk1">
                             <ul>
                                 <li>
                                     <span class="fa-stack fa-3x icon-span">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk">
                                         <h4>BOTOL 612ML</h4>
                                         <p> Dikemas dalam botol plastik yang kuat dan elegan.</p>
                                     </div>
                                 </li>
                                 <li>
                                     <span class="fa-stack fa-3x icon-span">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk">
                                         <h4>1 DUS ISI 12 BOTOL</h4>
                                         <p> 1 Dus Milagros Isi 12 Botol 612ml.</p>
                                     </div>
                                 </li>
                                 <li>

                                     <span class="fa-stack fa-3x icon-span">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk">
                                         <h4>2 SEGEL</h4>
                                         <p> Terdapat 2 segel di tutup botolnya. Segel plastik dan segel tutupnya.
                                         </p>
                                     </div>


                                 </li>
                             </ul>
                         </div>
                         <div class="col-md-4 tab-content-botol ">
                             <img src="assets/img/botol.png" alt="">
                         </div>
                         <div class="col-md-4 tab-content-produk2">
                             <ul>
                                 <li>

                                     <span class="fa-stack fa-3x icon-span2">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk2">
                                         <h4>BARCODE KEASLIAN</h4>
                                         <p> terdapat barcode di setiap kardus Milagros untuk cek keaslian.</p>
                                     </div>


                                 </li>
                                 <li>

                                     <span class="fa-stack fa-3x icon-span2 ">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk2">
                                         <h4>BERAT 7KG</h4>
                                         <p> Berat 1 dus milagros adalah 7KG.</p>
                                     </div>


                                 </li>
                                 <li>

                                     <span class="fa-stack fa-3x icon-span2">
                                         <i class=" fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-book fa-stack-1x  "></i></span>
                                     <div class="content-produk2">
                                         <h4>DESAIN BARU</h4>
                                         <p> Dus dan botol milagros kini dengan desasin baru.</p>
                                     </div>


                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="tab-pane fade mt-4 pb-4" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="container">
                     <div class="row">
                         <div class="col-md-6 tab-content-manfaat">
                             <ul>
                                 <li>
                                     <span class="fa-stack fa-3x icon-span3">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-paper-plane fa-stack-1x  "></i> </span>
                                     <div class="content-produk3">
                                         <h4>Anti Oksidan</h4>
                                         <p>Menghentikan proses perusakan sel di dalam tubuh.</p>
                                     </div>
                                 </li>
                                 <li>
                                     <span class="fa-stack fa-3x icon-span3 ">
                                         <i class="fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-paper-plane fa-stack-1x  "></i></span>
                                     <div class="content-produk3">
                                         <h4>Stable Alkali</h4>
                                         <p> Menyeimbakan Kelebihan kadar asam di dalam tubuh.</p>
                                     </div>
                                 </li>
                                 <li>
                                     <span class="fa-stack fa-3x icon-span3">
                                         <i class=" fas fa-circle fa-stack-2x" style="color: rgb(173, 31, 216);"></i>
                                         <i class="fas fa-paper-plane fa-stack-1x  "></i></span>
                                     <div class="content-produk3">
                                         <h4>Scalar Energy</h4>
                                         <p> Meningkatkan daya tahan & Stamina tubuh manusia.</p>
                                     </div>
                                 </li>
                             </ul>
                             <div class="text-tambahan">
                                 <h4>Milagros Untuk Semua</h4>
                                 <p>Milagros bisa diminum oleh balita sampai lansia bahakan bagus untuk ibu hamil</p>
                             </div>
                         </div>
                         <div class="col-md-6 content-botol">
                             <img src="assets/img/manfaat.png" alt="">
                         </div>
                     </div>
                 </div>


             </div>

         </div>
     </div>

 </div>



 <!-- carousel legalitas -->
 <div class="container">

     <div class="row">
         <!-- 
                =================================
                This Code Block is For PC Edition
                ================================= 
            -->

         <div id="carouselExample" class="carousel slide d-none d-sm-none d-md-block" data-ride="carousel">

             <div class="carousel-inner">

                 <div class="carousel-item active">

                     <div class="row">
                         <div class="col-md-4">
                             <img class="d-block w-100" src="assets/img/lab.jpg" alt="First slide">
                         </div>
                         <div class="col-md-4">
                             <img class="d-block w-100 " height="500" src="assets/img/medika.png" alt="First slide">
                         </div>
                         <div class="col-md-4">
                             <img class="d-block w-100" src="assets/img/mui.jpg" alt="First slide">
                         </div>

                     </div>

                 </div>
                 <!-- Carousel Item 1 -->

                 <div class="carousel-item">

                     <div class="row">
                         <div class="col-md-4">
                             <img class="d-block w-100" src="assets/img/sni.jpg" alt="First slide">
                         </div>

                         <div class="col-md-4">
                             <img class="d-block w-100" src="assets/img/apli.jpg" alt="First slide">
                         </div>
                         <div class="col-md-4">
                             <img class="d-block w-100" src="assets/img/bpom.jpg" alt="First slide">
                         </div>

                     </div>

                 </div>
                 <!-- Carousel Item 2 -->

                 <!-- <div class="carousel-item">

                        <div class="row">
                            <div class="col-md-3">
                                <img class="d-block w-100"
                                    src="https://fakeimg.pl/800x400/?retina=1&text=Logo 9&font=noto" alt="First slide">
                            </div>
                            <div class="col-md-3">
                                <img class="d-block w-100"
                                    src="https://fakeimg.pl/800x400/?retina=1&text=Logo 10&font=noto" alt="First slide">
                            </div>
                            <div class="col-md-3">
                                <img class="d-block w-100"
                                    src="https://fakeimg.pl/800x400/?retina=1&text=Logo 11&font=noto" alt="First slide">
                            </div>
                            <div class="col-md-3">
                                <img class="d-block w-100"
                                    src="https://fakeimg.pl/800x400/?retina=1&text=Logo 12&font=noto" alt="First slide">
                            </div>
                        </div>

                    </div> -->
                 <!-- Carousel Item 3 -->

             </div>

             <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">

                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                 <span class="sr-only">Previous</span>

             </a>
             <!-- Carousel Control Prev -->

             <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">

                 <span class="carousel-control-next-icon" aria-hidden="true"></span>

                 <span class="sr-only">Next</span>

             </a>
             <!-- Carousel Control Next -->

         </div>
         <!-- End of Carousel Example -->

         <!-- 
                =================================
                /End of PC Edition 
                =================================
            -->


         <!-- 
                =================================
                This Code Block is For Mobile Edition
                ================================= 
            -->

         <div id="carouselExampleMobile" class="carousel slide d-md-none d-lg-none d-xl-none" data-ride="carousel">

             <div class="carousel-inner">

                 <div class="carousel-item active">

                     <img class="d-block w-100" src="assets/img/lab.jpg" alt="First slide">

                 </div>
                 <!-- Carousel Item 1 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/medika.png" alt="Second slide">

                 </div>
                 <!-- Carousel Item 2 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mui.jpg" alt="Third slide">

                 </div>
                 <!-- Carousel Item 3 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/sni.jpg" alt="Fourth slide">

                 </div>
                 <!-- Carousel Item 4 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/apli.jpg" alt="Fifth slide">

                 </div>
                 <!-- Carousel Item 5 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/bpom.jpg" alt="Sixth slide">

                 </div>
                 <!-- Carousel Item 6 -->



             </div>

             <a class="carousel-control-prev" href="#carouselExampleMobile" role="button" data-slide="prev">

                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                 <span class="sr-only">Previous</span>

             </a>
             <!-- Carousel Control Prev -->

             <a class="carousel-control-next" href="#carouselExampleMobile" role="button" data-slide="next">

                 <span class="carousel-control-next-icon" aria-hidden="true"></span>

                 <span class="sr-only">Next</span>

             </a>
             <!-- Carousel Control Next -->

         </div>
         <!-- End of Carousel Example -->

         <!-- 
                =================================
                /End of Mobile Edition 
                =================================
            -->

     </div>
     <!-- End of Row -->

 </div>
 <!-- End of Container -->
 <!-- end -->

 <div class="container-fluid pt-4 demo-produk" style="background-color: rgb(52, 35, 68);">
     <h1 class="text-center pb-5" style="color: white;">DEMO PRODUK</h1>

     <div class="row">
         <?php foreach ($video as $vd) : ?>
             <div class="col-md-4 card-produk ">
                 <div class="card mb-4 box-shadow">
                     <!-- <iframe src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                     <iframe src="https://www.youtube.com/embed/<?= $vd['video_link']; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                 </div>
             </div>
         <?php endforeach; ?>
     </div>



 </div>

 </div>
 <div class="container-fluid  pembelian">
     <h1 class="text-center pt-3 pb-3 " style="color: white; ">PAKET PEMBELIAN</h1>
     <div class="row justify-content-center pb-5 ">
         <div class="card text-center mr-4 card-harga" style="width: 18rem;">
             <div class="card-body">
                 <h5 class="card-title">PELANGGAN</h5>
                 <h1>350.000,-</h1>
                 <p>Akan Mendapatkan 1 Hak Usaha untuk di jalankan Bisnis Plan Nya (tidak wajib)</p>
                 <p class="card-text">1 DUS MILAGROS </br>
                     1 KARTU PRODUK</br>
                     1 STARTERKIT</br>
                     1 KARTU MITRA USAHA</p>
                 <p class="card-text">POTENSI KOMISI</br>
                     Rp. 240.000,- / Hari</br>
                     Rp. 7.200.000,- / Bulan</p>

                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary ">Beli Sekarang</a>
             </div>
         </div>
         <div class="card text-center mr-4 card-harga" style="width: 18rem;">
             <div class="card-body">
                 <h5 class="card-title">GOLD</h5>
                 <h1>1.250.000,-</h1>
                 <p>Akan Mendapatkan 4 Hak Usaha untuk di jalankan Bisnis Plan Nya (tidak wajib)</p>
                 <p class="card-text">4 DUS MILAGROS</br>
                     4 KARTU PRODUK</br>
                     1 STARTERKIT</br>
                     1 KARTU MITRA USAHA</p>
                 <p class="card-text">POTENSI KOMISI</br>
                     Rp. 720.000,- / Hari</br>
                     Rp. 21.600.000,- / Bulan</p>
                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary">Beli Sekarang</a>
             </div>
         </div>
         <div class="card text-center mr-4 card-harga" style="width: 18rem;">
             <div class="card-body">
                 <h5 class="card-title">DIAMOND</h5>
                 <h1>2.150.000,-</h1>
                 <p>Akan Mendapatkan 7 Hak Usaha untuk di jalankan Bisnis Plan Nya (tidak wajib)</p>
                 <p class="card-text">7 DUS MILAGROS</br>
                     7 KARTU PRODUK</br>
                     1 STARTERKIT</br>
                     1 KARTU MITRA USAHA</p>
                 <p class="card-text">POTENSI KOMISI</br>
                     Rp. 1.6800.000,- / Hari</br>
                     Rp. 50.400.000,- / Bulan </p>

                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%20Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary">Beli Sekarang</a>
             </div>
         </div>
     </div>
 </div>
 <!-- tetstimoni -->
 <div style="background-color: rgb(52, 35, 68); ">
     <div class="container " style="padding-top: 2%;">
         <div style="text-align: center; color: white; padding-bottom: 2%;">
             <h1>TESTIMONI</h1>
         </div>
         <div class="row ">
             <div class="col-md-4 card-produk ">
                 <div class="card mb-4 box-shadow">
                     <img height="250" src="assets/img/Testimoni 1.jpeg" class="card-img-top" alt="...">


                 </div>
             </div>
             <div class="col-md-4 card-produk">
                 <div class="card mb-4 box-shadow">
                     <img height="250" src="assets/img/Testimoni 2.jpeg" class="card-img-top" alt="...">


                 </div>
             </div>
             <div class="col-md-4 card-produk">
                 <div class="card mb-4 box-shadow">
                     <img height="250" src="assets/img/Testimoni 3.jpeg" class="card-img-top" alt="...">
                 </div>
             </div>

             <div class="col-md-4 card-produk">
                 <div class="card mb-4 box-shadow">
                     <img height="250" src="assets/img/Testimoni 1.jpeg" class="card-img-top" alt="...">

                 </div>
             </div>
             <div class="col-md-4 card-produk">
                 <div class="card mb-4 box-shadow">
                     <img height="250" src="assets/img/Testimoni 2.jpeg" class="card-img-top" alt="...">

                 </div>
             </div>
             <div class="col-md-4 card-produk">
                 <div class="card mb-4 box-shadow">
                     <img height="250" width="" src="assets/img/Testimoni 3.jpeg" class="card-img-top" alt="...">

                 </div>
             </div>




         </div>
         <div class="row justify-content-center pb-4"> <a href="testimoni.html" class="btn btn-primary ">Show
                 More</a>
         </div>
     </div>
 </div>
 <!-- join -->
 <div class="join">

     <div class="container ">
         <div class="row justify-content-center pt-4 pb-4">
             <div class="col-md-6 align-self-center text">
                 <h3 style="color: white;">Produk baru dari Milagros

                     Jika ingin merasakan kulit wajah jadi makin cerah,halus, kenyal,kencang dan Glowing alami tanpa
                     Harus ribet ini itu,cukup hanya dengan di semprot aja perbedaan nya sdh bisa dirasakan dalam
                     waktu 1-2 menit aja.
                 </h3>
                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%20Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary">Chat Sekarang</a>
             </div>
             <div class="col-md-6">
                 <img src="assets/img/mara.png" height="500" alt="join milagros">
             </div>
         </div>
     </div>

 </div>

 <!-- end testimoni -->
 <!-- delivery -->
 <!-- carousel delivery -->
 <div class="container ">

     <div class="row pb-4 ml-4">
         <div class="col-md-12">

             <h1 class="text-center carousel-title" style="color: white;">DELIVERY</h1>
             <hr>

         </div>
         <!-- 
            =================================
            This Code Block is For PC Edition
            ================================= 
        -->

         <div id="carouselDelivery" class="carousel slide d-none d-sm-none d-md-block" data-ride="carousel">

             <div class="carousel-inner">

                 <div class="carousel-item active">

                     <div class="row">
                         <div class="col-md-3">
                             <img class="d-block w-100" src="assets/img/mg/gambar1.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100 " height="340" src="assets/img/mg/gambar2.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar3.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar4.png" alt="First slide">
                         </div>
                     </div>

                 </div>
                 <!-- Carousel Item 1 -->

                 <div class="carousel-item">

                     <div class="row">
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar5.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar6.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar7.png" alt="First slide">
                         </div>
                         <div class="col-md-3">
                             <img class="d-block w-100" height="340" src="assets/img/mg/gambar8.png" alt="First slide">

                         </div>

                     </div>

                 </div>
                 <!-- Carousel Item 2 -->

                 <!-- <div class="carousel-item">

                    <div class="row">
                        <div class="col-md-3">
                            <img class="d-block w-100"
                                src="https://fakeimg.pl/800x400/?retina=1&text=Logo 9&font=noto" alt="First slide">
                        </div>
                        <div class="col-md-3">
                            <img class="d-block w-100"
                                src="https://fakeimg.pl/800x400/?retina=1&text=Logo 10&font=noto" alt="First slide">
                        </div>
                        <div class="col-md-3">
                            <img class="d-block w-100"
                                src="https://fakeimg.pl/800x400/?retina=1&text=Logo 11&font=noto" alt="First slide">
                        </div>
                        <div class="col-md-3">
                            <img class="d-block w-100"
                                src="https://fakeimg.pl/800x400/?retina=1&text=Logo 12&font=noto" alt="First slide">
                        </div>
                    </div>

                </div> -->
                 <!-- Carousel Item 3 -->

             </div>

             <a class="carousel-control-prev" href="#carouselDelivery" role="button" data-slide="prev">

                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                 <span class="sr-only">Previous</span>

             </a>
             <!-- Carousel Control Prev -->

             <a class="carousel-control-next" href="#carouselDelivery" role="button" data-slide="next">

                 <span class="carousel-control-next-icon" aria-hidden="true"></span>

                 <span class="sr-only">Next</span>

             </a>
             <!-- Carousel Control Next -->

         </div>
         <!-- End of Carousel Example -->

         <!-- 
            =================================
            /End of PC Edition 
            =================================
        -->


         <!-- 
            =================================
            This Code Block is For Mobile Edition
            ================================= 
        -->

         <div id="carouselDeliveryMobile" class="carousel slide d-md-none d-lg-none d-xl-none" data-ride="carousel">

             <div class="carousel-inner">

                 <div class="carousel-item active">

                     <img class="d-block w-100" src="assets/img/mg/gambar1.png" alt="First slide">

                 </div>
                 <!-- Carousel Item 1 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar2.png" alt="Second slide">

                 </div>
                 <!-- Carousel Item 2 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar3.png" alt="Third slide">

                 </div>
                 <!-- Carousel Item 3 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar4.png" alt="Fourth slide">

                 </div>
                 <!-- Carousel Item 4 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar5.png" alt="Fifth slide">

                 </div>
                 <!-- Carousel Item 5 -->

                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar6.png" alt="Sixth slide">

                 </div>
                 <!-- Carousel Item 6 -->
                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar7.png" alt="Sixth slide">

                 </div>
                 <!-- Carousel Item 7 -->
                 <div class="carousel-item">

                     <img class="d-block w-100" src="assets/img/mg/gambar8.png" alt="Sixth slide">

                 </div>
                 <!-- Carousel Item  -->



             </div>

             <a class="carousel-control-prev" href="#carouselDeliveryMobile" role="button" data-slide="prev">

                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                 <span class="sr-only">Previous</span>

             </a>
             <!-- Carousel Control Prev -->

             <a class="carousel-control-next" href="#carouselDeliveryMobile" role="button" data-slide="next">

                 <span class="carousel-control-next-icon" aria-hidden="true"></span>

                 <span class="sr-only">Next</span>

             </a>
             <!-- Carousel Control Next -->

         </div>
         <!-- End of Carousel Example -->

         <!-- 
            =================================
            /End of Mobile Edition 
            =================================
        -->

     </div>
     <!-- End of Row -->

 </div>
 <!-- End of Container -->
 <!-- end -->
 <!-- end delivery -->

 <!-- join -->
 <div class="join">

     <div class="container ">
         <div class="row justify-content-center pt-4 pb-4">
             <div class="col-md-6 align-self-center text">
                 <h3 style="color: white;">Bergabunglah bersama team solid kami, dan segera wujudkan impian untuk
                     masa
                     depan keluarga anda.
                 </h3>
                 <a href="https://wa.me/6282178603781/?text=ADMIN%20Saya%20Mau%20Daftar/Beli%20MILAGROS%20Nama%20Saya:%20%20Alamat:%20%20Terimakasih" class="btn btn-primary">Bergabung Sekarang</a>
             </div>
             <div class="col-md-6">
                 <img src="assets/img/join.jpeg" height="500" alt="join milagros">
             </div>
         </div>
     </div>

 </div>