<?php
class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
		$this->m_pengunjung->count_visitor();
	}
	public function index()
	{
		$data['video'] = $this->M_video->get_all_video();
		// var_dump($data['video']);
		// die;
		$data['title'] = "Selamat Datang";
		$this->load->view('templates/header', $data);
		$this->load->view('home/index');
		$this->load->view('templates/footer');
	}

	public function aboutus()
	{
		$data['title'] = "About Us";
		$this->load->view('templates/header', $data);
		$this->load->view('home/aboutus');
		$this->load->view('templates/footer');
	}

	function admin()
	{
		$x['post'] = $this->m_tulisan->get_post_home();
		$this->load->view('admin/v_dashboard', $x);
	}
}
