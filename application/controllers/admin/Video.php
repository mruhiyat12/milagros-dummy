<?php
class Video extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['logged_in'])) {
            $url = base_url('administrator');
            redirect($url);
        };
        $this->load->model('m_video');
    }


    function index()
    {
        $x['data'] = $this->m_video->get_all_video();
        $this->load->view('admin/v_video', $x);
    }

    function simpan_video()
    {

        $video_judul = strip_tags($this->input->post('xjudul'));
        $video_link = strip_tags($this->input->post('xlink'));
        $video_keterangan = strip_tags($this->input->post('xketerangan'));
        $this->m_video->simpan_video($video_judul, $video_link, $video_keterangan);
        echo $this->session->set_flashdata('msg', 'success');
        redirect('admin/video');
    }

    function update_video()
    {
        $kode = $this->input->post('kode');
        $video_judul = strip_tags($this->input->post('xjudul'));
        $video_link = strip_tags($this->input->post('xlink'));
        $video_keterangan = strip_tags($this->input->post('xketerangan'));
        $this->m_video->update_video($kode, $video_judul, $video_link, $video_keterangan);
        echo $this->session->set_flashdata('msg', 'info');
        redirect('admin/video');
    }
    function hapus_video()
    {
        $kode = strip_tags($this->input->post('kode'));
        $this->m_video->hapus_video($kode);
        echo $this->session->set_flashdata('msg', 'success-hapus');
        redirect('admin/video');
    }
}
