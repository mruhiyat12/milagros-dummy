<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Testimoni extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
    }

    public function index()
    {
        $data['title'] = "Testimoni";
        $this->load->view('templates/header', $data);
        $this->load->view('testimoni/index');
        $this->load->view('templates/footer');
    }
}
