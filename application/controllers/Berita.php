<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
    }

    public function berita()
    {
        $data['title'] = "Berita";
        $this->load->view('templates/header', $data);
        $this->load->view('content/berita');
        $this->load->view('templates/footer');
    }
    public function content()
    {
        $data['title'] = "Berita";
        $this->load->view('templates/header', $data);
        $this->load->view('content/content_berita');
        $this->load->view('templates/footer');
    }
}
